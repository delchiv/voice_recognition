import os
import sys
import ssl
import datetime
ssl._create_default_https_context = ssl._create_unverified_context

import speech_recognition as sr
r = sr.Recognizer()


def recognize(fn, provider, **kwargs):
    t = datetime.datetime.now()
    af = sr.AudioFile(fn)
    with af as source:
        r.adjust_for_ambient_noise(source)
        audio = r.record(source)
    try:
        res = provider(audio, **kwargs)
    except:
        res = "error"
    return datetime.datetime.now() - t, res


if __name__ == "__main__":
    if len(sys.argv) == 1:
        print("Usage: python recognize.py file.wav")
    else:
        fn = os.path.abspath(sys.argv[1])
        if os.path.exists(fn):
            time, res = recognize(fn, r.recognize_bing, key='1f64b2d34cea406ba889ceb29f61f587', language='ru-RU')
            print("bing: (%s) %s" % (time, res))

            time, res = recognize(fn, r.recognize_google, language='ru')
            print("google: (%s) %s" % (time, res))

            time, res = recognize(fn, r.recognize_sphinx, language='ru-RU')
            print("sphinx offline: (%s) %s" % (time, res))
        else:
            print("Error: file not found.")